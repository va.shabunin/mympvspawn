# mympv-spawn

## About

Module that provides binary to run mpv instance with exposed ipc interface.

## Usage

```
mympvspawn
```

## Configuration

Explicitely edit `/usr/lib/node_modules/mympvspawn/bin/mympvspawn.js`.

```js
// user-defined parameters here
let params = {
  socketFile: '/home/user/mpv.sock', // default: $XDG_RUNTIME_DIR/mympv.sock
  args: [] // default: ['--no-video', '--load-scripts=no']
  debug: true // default: false
};
```
