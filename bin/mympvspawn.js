#!/usr/bin/env node
const MpvSpawn = require('../index.js');

// user-defined parameters here
let params = {debug: false};

let myMpvSpawn = MpvSpawn(params).spawn();
