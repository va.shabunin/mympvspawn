// responds for mpv child process
const {spawn, exec} = require('child_process');

const MpvInstance = params => {
  let _params = {
    socketFile: `${process.env['XDG_RUNTIME_DIR']}/mympv.sock`,
    args: ['--no-video', '--load-scripts=no'],
    debug: true
  };
  Object.assign(_params, params);

  let self = {};
  let _debug = _params.debug;
  let _socketFile = _params.socketFile;
  let _args = _params.args;

  self.debug = (...args) => {
    if (_debug) {
      console.log('MpvSpawn::', args.map(t => t.toString()));
    }
  };

  self.spawn = _ => {
    _args.push(`--input-ipc-server=${_socketFile}`);
    _args.push(`--idle=yes`);
    self.debug(`Starting mpv with args: ${JSON.stringify(_args)}`);
    let mpvspawn = spawn('mpv', _args);
    mpvspawn.stdout.on('data', self.debug);
    mpvspawn.stderr.on('data', self.debug);
    mpvspawn.on('close', code => {
      self.debug(`Process exited with code ${code}`);
    });

    return mpvspawn;
  };

  return self;
};

module.exports = MpvInstance;
